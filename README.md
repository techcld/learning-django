# Introdução 

Este curso se dedica a fornecer conceitos básicos sobre o Django para programadores iniciantes. 
Durante o curso os participantes criarão um sistema de estoque simples, aplicando os conceitos básicos do django.

Durante este curso serão abordados conceitos como: 

* applications
* authentication
* mixins
* template tags
* template filters
* CRUD
* debug

## Arsenal ##

|Estória de Usuário|
| ----------- |
| **Título:**  Controle do Arsenal dos Vingadores| 
| **Porquê ?**| 
| preciso saber o que tem e o que não tem no arsenal do Vingadores| 
| **Quem ?**| 
| Happy Hogan guarda costas dos Vingadores| 
| **O que ?**| 
| quero conseguir controlar a quantidade e quais armas, armaduras e equipamentos existem no arsenal dos Vingadores| 
